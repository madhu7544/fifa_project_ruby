require 'csv'
require 'json'

def matches_won_per_team(matches)
    winning_teams_count = matches.each_with_object(Hash.new(0)) do |match, counts|
      home_team_goals = match['Home Team Goals'].to_i
      away_team_goals = match['Away Team Goals'].to_i
      winner_team = if home_team_goals > away_team_goals
                      match['Home Team Name']
                    elsif home_team_goals < away_team_goals
                      match['Away Team Name']
                    else
                      nil
                    end
      if winner_team
        counts[winner_team] += 1
      end
    end
    winning_teams_count.to_json
  end
  
matches = []

CSV.open('./data/WorldCupMatches.csv',encoding: 'UTF-8', col_sep: ',', headers: true) do |csv|
    csv.each do |row|
        matches << row.to_h
    end 
end

puts matches_won_per_team(matches)
