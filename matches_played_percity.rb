require 'csv'
require 'json'

def matches_per_city(matches)
  filter_cities = matches.select { |match| match['City'] != nil }
  cities = filter_cities.map { |match| match['City'].strip }

  matches_per_city = cities.each_with_object(Hash.new(0)) do |city, counts|
    counts[city] += 1
  end
  json_data = matches_per_city.to_json
end


matches = []
CSV.open('./data/WorldCupMatches.csv', encoding: 'UTF-8', col_sep: ',', headers: true) do |csv|
  csv.each do |row|
    matches<< row.to_h
  end
end

puts matches_per_city(matches)