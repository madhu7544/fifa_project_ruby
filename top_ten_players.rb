require 'csv'
require 'json'

def topTenPlayers(players)
    player_goals = players.each_with_object(Hash.new(0)) do |player, acc|
      events = player['Event'].to_s.split('')
      goals = events.count { |event| event.include?('G') }
      if goals != 0
        if acc[player['Player Name']]
          acc[player['Player Name']] += goals
        else
          acc[player['Player Name']] = goals
        end
      end
    end
  
    player_matches = players.each_with_object(Hash.new(0)) do |player, acc|
      acc[player['Player Name']] += 1
    end
  
    probability_of_player = player_goals.each_with_object(Hash.new(0)) do |(player_name, goals), acc|
      acc[player_name] = goals.to_f / player_matches[player_name]
    end
  
    top_players = probability_of_player.sort_by { |_player_name, probability| -probability }.to_h
    top_10_players = top_players.first(10).to_h   
    top_10_players.to_json
end
  
players = []

CSV.open('./data/WorldCupPlayers.csv', encoding: 'UTF-8', col_sep: ',', headers: true) do |csv|
    csv.each do |row|
        players << row.to_h 
    end
end
  
puts topTenPlayers(players)