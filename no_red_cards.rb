require 'csv'
require 'json'

def no_of_redCards(matches, players, year)
    match_ids = matches
      .select { |match| match['Year'] == year.to_s}
      .map { |match| match['MatchID'] }

    red_cards_per_team = players.each_with_object(Hash.new(0)) do |player, counts|
        if player['Event'] != nil
            if player['Event'].include?('R') && match_ids.include?(player['MatchID'])
                team_name = player['Team Initials']
                counts[team_name] += 1
            end
         end
    end
    red_cards_per_team.to_json
end

matches = []

CSV.open('./data/WorldCupMatches.csv', encoding: 'UTF-8', col_sep: ',', headers: true) do |csv|
  csv.each do |row|
    matches << row.to_h 
  end 
end

players = []

CSV.open('./data/WorldCupPlayers.csv', encoding: 'UTF-8', col_sep: ',', headers: true) do |csv|
  csv.each do |row|
    players << row.to_h 
  end 
end

puts no_of_redCards(matches,players,2014)


